from conans import ConanFile, tools


class P0829Conan(ConanFile):
    name = "p0829"
    version = "9.0"
    license = "GPL"
    author = "Paul M. Bendixen <paulbendixen@gmail.com>"
    url = "https://gitlab.com/avr-libstdcxx"
    description = "Package for using freestanding proposal P0829"
    topics = ("Standards proposal", "Freestanding")
    no_copy_source = True
    settings = "os", "arch", "compiler"

    def source(self):
        git = tools.Git()
        git.clone( 'https://gitlab.com/avr-libstdcxx/gcc.git', 'p0829' )

    def configure(self):
        if self.settings.compiler != "gcc":
            raise ConanInvalidConfiguration("This package is only valid for GCC")
        if self.settings.compiler.version != 9:
            raise ConanInvalidConfiguration("This package is only valid for GCC 9")

    def package(self):
        bits = ["algorithmfwd.h" , "boost_concept_check.h" , "char_traits.h "
        , "concept_check.h" , "cpp_type_traits.h" , "functexcept.h"
        , "functional_hash.h " , "hash_byte.h" , "invoke.h" , "parse_numbers.h"
        , "postypes.h" , "predefined_ops.h " , "ptr_traits.h" , "range_access.h"
        , "refwrap.h" , "std_abs.h" , "stl_algo.h " , "stl_algobase.h"
        , "stl_function.h" , "stl_heap.h" , "stl_iterator.h "
        , "stl_iterator_base_funcs.h" , "stl_iterator_base_types.h "
        , "stl_numeric.h" , "stl_pair.h" , "stl_relops.h " , "uniform_int_dist.h"
        , "uses_allocator.h" ]
        for lib in bits:
            self.copy( lib, src="libstdc++-v3/include/bits", dst="include/bits")
        libraries = [ "algorithm", "array", "bitset", "chrono", "charconv", "functional",
                        "iterator", "string", "string_view", "system_error", "utility",
                        "numeric", "optional", "tuple", "ratio" ]
        for lib in libraries:
            self.copy( lib, src="libstdc++-v3/include/std", dst="include" )

        if self.settings.os != "Windows":
            error_constants_dir = "libstdc++-v3/config/os/generic/"
        else: #MinGW
            if self.settings.arch == "x86":
                error_constants_dir = "libstdc++-v3/config/os/mingw32/"
            else: #MinGW64
                error_constants_dir = "libstdc++-v3/config/os/mingw32-w64/"
        self.copy( "error_constants.h", src=error_constants_dir, dst = "include/bits" )
        cLibs = [ "csetjmp", "cwchar", "cerrno", "cmath", "ctime", "cctype" ]
        for lib in cLibs:
            self.copy( lib, src="libstdc++-v3/include/c_std", dst="include" )
        self.copy( "type_traits.h", src="libstdc++-v3/include/ext", dst="include/ext" )
        self.copy( "numeric_traits.h", src="libstdc++-v3/include/ext", dst="include/ext" )
        self.copy( "debug.h", src="libstdc++-v3/include/debug", dst="include/debug" )
        self.copy( "assertions.h", src="libstdc++-v3/include/debug", dst="include/debug" )
        self.copy( "binders.h", src="libstdc++-v3/include/backward", dst="include/backward" )


    def package_info(self):
        # Make sure the -ffreestanding option is set
        self.cpp_info.cxxflags = ["-ffreestanding"]
        self.info.header_only()
